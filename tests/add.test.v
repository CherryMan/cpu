`include "add.v"

module Test;

    reg sub;
    reg [15:0] a, b;
    wire [16:0] out;

    AddSub16 adder(sub, a, b, out);

    task set;
        input [15:0] ai, bi;

        if (1) begin
            a <= ai;
            b <= bi;
        end
    endtask

    task test;
        input reg [15:0] sum;

        #100 // Wait for everything to be set

        if (! (sum === out[15:0])) begin
            $display("Failure in AddSub16 module with values:");
            $display("  a:     %b", a);
            $display("  b:     %b", b);
            $display("  out:  %b", out);
            $display("");
            $display("EXPECTED:");
            $display("  out:   %b", sum);
            $display("");
            $display("");
        end
    endtask

    initial begin
        sub = 0;

        set(0, 0);
        test(0);

        set(1000, 1);
        test(1001);

        sub = 1;

        set(1, 1);
        test(0);

        set(0, -1);
        test(1);

        set(0, 1);
        test(-1);
    end

endmodule
