`include "cpu.v"
`include "io.v"

module Computer(clk, reset, next, pin_in, pin_out);
    parameter DATA_WIDTH = 16;

    (* mark_debug = "true" *) input [15:0] pin_in;
    (* mark_debug = "true" *) output [15:0] pin_out;

    (* mark_debug = "true" *) input clk;
    (* mark_debug = "true" *) input reset;
    (* mark_debug = "true" *) input next;

    //assign pin_out[11] = reset;
    //assign pin_out[12] = next;

    //reg clk;
    //reg reset;
    //reg next;
    //integer i;
    //initial
    //begin
        //$monitor("PIN OUT UPDATED: %b", pin_out);

        //pin_in = 16'hfff0;

        //#10 clk = 0;
        //#10 next = 1;

        //for (i = 0; i < 32; i = i + 1)
        //begin
            //#100 clk = 0;
            //#100 clk = 1;
        //end
    //end

    wire mem_write;
    wire [(DATA_WIDTH - 1):0] mem_out, mem_in, mem_addr;

    CPU #(DATA_WIDTH) cpu
        (clk, reset, next,
         mem_out, mem_write, mem_addr, mem_in);

    IO #(DATA_WIDTH, 16) io
        (clk,
         mem_write, mem_in, mem_addr, mem_out,
         pin_in, pin_out);
endmodule
