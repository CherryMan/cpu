init:
li	ip, 0
li	op, 0
li	sp, 0
lui	ip, 128
li	t0, 16
add	op, t0, ip
li	t0, 0
li	t1, 1

# Turn off main LED
sw	t0, 13(op)
sw	t0, 14(op)
sw	t0, 15(op)

# Turn off active LED
sw	t0,	0(op)

lju	loop
loop:

# Check button
lw	s0, 0(ip)
bne	s0, LED_on

LED_off:
sw	t0, 0(op)
j	loop

LED_on:
sw	t1, 0(op)
j	loop
