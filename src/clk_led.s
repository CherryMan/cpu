begin:
li	ip, 0
li	op, 0
li	sp, 0
lui	ip, 128
li	t0, 16
add	op, t0, ip
# ip points to input
# op points to output
# stack pointer initialised

# 0 and 1 values
li	t0,	0
li	t1,	1

sw	t1,	0(op)
#sw	t1,	1(op)
#sw	t1,	11(op)
sw	t1,	12(op)
sw	t1,	13(op)
sw	t1,	14(op)
sw	t1,	15(op)

lju	loop
loop: # primary loop

# ---- SLEEP BEGIN
li	s0, 16

wait_0:
li	ra, -1
lui	ra, -1

wait_1:
sub	ra, ra, t1
bne	ra, wait_1

wait_2:
sub	s0, s0, t1
bne	s0, wait_0
# ---- SLEEP END

# Toggle
lw	s0,	0(op)
bne	s0,	LED_off

LED_on:
sw	t1,	0(op)
j	loop

LED_off:
sw	t0,	0(op)
j	loop
