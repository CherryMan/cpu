# Instruction Set

## Load		01

lw	000	rd	rf	5b-off
slt	010	rd	rf	rs	xx
sgt	011	rd	rf	rs	xx
li	100	rd	8b
lui	101	rd	8b


## Store	10

lju	11	xxxx	8b-addr
sw	00	x	rd	rf	5b-off


## Arithmetic	00

nop	000	xxx	rf	xxx	xx
or	001	rd	rf	rs	xx
nor	010	rd	rf	rs	xx
xor	011	rd	rf	rs	xx
and	100	rd	rf	rs	xx
add	101	rd	rf	rs	xx
sub	110	rd	rf	rs	xx


## Jump/Branch	11

j	00	0	xxx	8b-addr
jr	00	1	rf	xxxxxxxx
jeq	01	0	rs	8b-addr
jne	01	1	rs	8b-addr

beq	10	rs	9bs-off
bne	11	rs	9bs-off


# Pseudo-instructions

push
pop


# Registers

000:	0	io
001:	1	ra
010:	2	t0
011:	3	t1
100:	4	s0
101:	5	s1
110:	6	sp
111:	7	ip

<!-- vim: set noet ts=8 sts=8 sw=8: -->
