.POSIX:

MKASM	= bin/genasm
VC	= iverilog
VFLAGS	= -Isrc

.PHONY:	all

all: cpu

cpu: src/rom.v
	$(VC) $(VFLAGS) -o $@ $<

src/rom.v: src/rom.s
	$(MKASM) $< $@
